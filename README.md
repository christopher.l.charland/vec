# c-vec

c-vec is a C library that implements a dynamically sized array.

## Vec
The Vec type provides a dynamically sized array with bounds checked access to data stored within.


## T_Vec
This is the thread safe version of the Vec type.  All of the functions have identical signatures, the data structure is simply wrapped in a mutex lock and two extra functions are provided to the user.  T_Vec_lock and T_Vec_unlock are provided for the creation of thread safe abstractions on top of the T_Vec structure.