/**
 * @details
 * This library provides a dynamically sized array type Vec
 * that provides bounds checked access to data stored within.
 * \n\n
 * All data stored in a single instantiation must be of the same type.
 * \n\n
 * Access through functions that end in _ref MUST be scrutinized.
 * The pointers returned are to the underlying data structure and may be
 * invalidated by modifications or freeing of the Vec.
 */

#ifndef VEC
#define VEC

/** @file */

#include <stdlib.h>

typedef struct vector Vec;

typedef int(*FindFunc)(void *);
typedef int(*MapFunc)(void *);

/**
 * This function initializes a new Vec
 * @param item_size The "sizeof" of the type that this Vector is to hold
 * @return A newly allocated Vec initialized to DEFAULT_CAPACITY
 */
Vec*
Vec_init(size_t item_size);

/**
 * This function deallocates all resources associated with the
 * Vec data structure. If it contained pointers they should be free'd
 * prior to calling this function.
 * @param v The Vector to destroy
 * @return SUCCESS
 */
int
Vec_destroy(Vec *v);

/**
 * This function pushes a new item onto the end of the Vec.
 * if necessary the Vec will expand to accommodate
 * @param v The Vector to append the new item with
 * @param push_data Pointer to the data that will be appended to the Vector
 * @return SUCCESS
 */
int
Vec_push(Vec *v, void *push_data);

/**
 * This function allocates space for a copy of the last item in the vector, copies it
 * then overwrites that last spot with 0's
 * @param v The Vector from which to pop an item
 * @return a newly allocated copy of the last item in the Vector
 */
void *
Vec_pop(Vec *v);

/**
 * This function gets a pointer to an item identified by index
 * @param v The Vec to pull a reference out of
 * @param idx the index of the item to get
 * @return A pointer to the item at idx
 */
void *
Vec_fetch_ref(Vec *v, size_t idx);

/**
 * This function applies the supplied function to
 * each item in the Vec.
 * @param v The Vec to iterate over
 * @param map_func The function that will be applied to each item
 * @return -1 on SUCCESS (yes I know).  When the function being applied
 * has a non-zero return Vec_map will return the index of the item that errored.
 */
int
Vec_map(Vec *v, MapFunc map_func);

/**
 * This function uses a finder function to get an item that meets a criteria.
 * @param v The Vec to iterate over
 * @param find_func The function that will be used to find a result.
 * @return A pointer to the found item on success, NULL if no item was found.
 */
void *
Vec_find_ref(Vec *v, FindFunc find_func);

/**
 * This function uses a finder function to get the index of an item that meets a criteria.
 * @param v The Vec to iterate over
 * @param find_func The function that will be used to find a result.
 * @return A ssize_t representing the index of the found item, -1 if no result was found.
 */
ssize_t
Vec_find_idx(Vec *v, FindFunc find_func);


int
vec_enlarge(Vec *v);


#endif //VEC