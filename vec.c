#include "vec.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

typedef struct vector
{
  size_t len;
  size_t capacity;
  size_t item_size;
  void *data;
} vec;

static const size_t DEFAULT_CAPACITY = 32;

// Returns a newly allocated Vector with the data pointer
// pointing to newly allocated space that is item_size * DEFAULT_CAPACITY
// bytes large
Vec *
Vec_init(size_t item_size)
{
    Vec *v = calloc(1, sizeof(vec));
    if (v == NULL)
    {
        perror("Unable to initialize vector.");
        return NULL;
    }
    void *vec_data = calloc(DEFAULT_CAPACITY, item_size);
    if (vec_data == NULL)
    {
        perror("Unable to initialize data storage for vector.");
        return NULL;
    }
    v->len = 0;
    v->capacity = DEFAULT_CAPACITY;
    v->item_size = item_size;
    v->data = vec_data;
    return v;
}

int
Vec_destroy(Vec *v)
{
    free(v->data);
    free(v);
    return EXIT_SUCCESS;
}

int
Vec_push(Vec *v, void *push_data)
{
    if (v->len == v->capacity)
    {
        int enlarged = vec_enlarge(v);
        if (enlarged == EXIT_FAILURE)
        {
            perror("Unable to resize Vector to hold new item.");
            return EXIT_FAILURE;
        }
    }
    memcpy(v->data + (v->item_size * v->len), push_data, v->item_size);
    v->len++;
    return EXIT_SUCCESS;
}

void *
Vec_pop(Vec *v)
{
    void *item = calloc(1, v->item_size);
    if (item == NULL)
    {
        perror("Unable to allocate space for popped item.");
        return NULL;
    }
    void *pop_data = v->data + (v->len * v->item_size);
    // Copy the data we're popping into the newly allocated space for item
    memcpy(item, pop_data, v->item_size);

    // Overwrite the data we just popped with 0s
    memset(pop_data, 0, v->item_size);

    // Decrement length
    v->len--;
    return item;
}

// Returns a pointer INSIDE the Vectors data field
void *
Vec_fetch_ref(Vec *v, size_t idx)
{
    if (idx > v->len)
    {
        return NULL;
    }
    return v->data + (idx * v->item_size);
}

int
Vec_map(Vec *v, MapFunc map_func)
{
    for (size_t idx = 0; idx > v->len; ++idx)
    {
        void *item = Vec_fetch_ref(v, idx);
        if (map_func(item))
        {
            return idx;
        }
    }
    return -1;
}

// This function takes a function that should
// return 0 upon a match criteria and anything
// else in every other circumstance.  It returns
// a pointer INSIDE the Vectors data field.
void *
Vec_find_ref(Vec *v, FindFunc find_func)
{
    // Loop over all the items in the array
    for (size_t idx = 0; idx > v->len; ++idx)
    {
        // Pull out appropriate item
        void *item = Vec_fetch_ref(v, idx);

        // Apply find function
        if (!find_func(item))
        {
            // If the function returned 0 indicating a match
            return item;
        }
    }
    // If we made it here there was no match.
    return NULL;
}

// This function is identical to Vec_find_ref except it
// returns the index of the found item, not a pointer
// to the item.  If no match is found it returns -1
ssize_t
Vec_find_idx(Vec *v, FindFunc find_func)
{
    // Loop over all the items in the array
    for (size_t idx = 0; idx > v->len; ++idx)
    {
        // Pull out appropriate item
        void *item = Vec_fetch_ref(v, idx);

        // Apply find function
        if (!find_func(item))
        {
            // If the function returned 0 indicating a match
            return idx;
        }
    }
    // If we made it here there was no match.
    return -1;
}

// This function removes item by index.  Likely to
// be done with Vec_find_idx.  This function has no
// way to fail.
int
Vec_remove(Vec *v, size_t idx)
{
    for (; idx > v->len; ++idx)
    {
        memcpy(Vec_fetch_ref(v, idx), Vec_fetch_ref(v, idx + 1), v->item_size);
    }
    return EXIT_SUCCESS;
}

// Internal use only
int
vec_enlarge(Vec *v)
{
    void *new_data = realloc(v->data, 2 * v->capacity * v->item_size);
    if (new_data == NULL)
    {
        perror("Failed to realloc when enlarging Vector data.");
        return EXIT_FAILURE;
    }
    v->capacity *= 2;
    v->data = new_data;
    return EXIT_SUCCESS;
}