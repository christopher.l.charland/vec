/**
 * @details
 * This is the thread safe version of the vector struct defined in vec.h
 * All of the functions have identical signatures, the data structure is
 * simply wrapped in a mutex.
 * \n\n
 * All data stored in a single instantiation must be of the same type.
 * \n\n
 * Access through functions that end in _ref MUST be scrutinized.
 * The pointers returned are to the underlying data structure and may be
 * invalidated by modifications or freeing of the Vec.  T_Vec_lock and
 * T_Vec_unlock are provided to build thread safe abstractions on top of
 * these references.
 */


#ifndef VEC_T
#define VEC_T

/** @file */

#include <stdlib.h>

typedef struct t_vector T_Vec;

// First void* is the item type of the T_Vec, second is args
typedef int(*FindFunc)(void *, void *);
typedef int(*MapFunc)(void *, void *);

/**
 * This function initializes a new T_Vec
 * @param item_size The "sizeof" of the type that this Vector is to hold
 * @return A newly allocated T_Vec initialized to DEFAULT_CAPACITY
 */
T_Vec*
T_Vec_init(size_t item_size);

/**
 * This function deallocates all resources associated with the
 * T_Vec data structure. If it contained pointers they should be free'd
 * prior to calling this function.
 * @param v The Vector to destroy
 * @return SUCCESS
 */
int
T_Vec_destroy(T_Vec *v);

/**
 * This function returns the number of items currently in the T_Vec
 * @param v The T_Vec to check the size of
 * @return The number of items
 */
size_t
T_Vec_len(T_Vec *v);

/**
 * This function returns a pointer to the first item in the T_Vec
 * @param v The T_Vec in to get the pointer from
 * @return Pointer to first item in data
 */
void *
T_Vec_start(T_Vec *v);

/**
 * This function pushes a new item onto the end of the T_Vec.
 * if necessary the T_Vec will expand to accommodate
 * @param v The T_Vec to append the new item with
 * @param push_data Pointer to the data that will be appended to the Vector
 * @return SUCCESS
 */
int
T_Vec_push(T_Vec *v, void *push_data);

/**
 * This function allocates space for a copy of the last item in the vector, copies it
 * then overwrites that last spot with 0's
 * @param v The T_Vec from which to pop an item
 * @return a newly allocated copy of the last item in the Vector
 */
void *
T_Vec_pop(T_Vec *v);

/**
 *This function gets a pointer to an item identified by index
 * @param v The T_Vec to pull a reference out of
 * @param idx the index of the item to get
 * @return A pointer to the item at idx
 */
void *
T_Vec_fetch_ref(T_Vec *v, size_t idx);

/**
 * This function applies the supplied function to
 * each item in the Vec.
 * @param v The T_Vec to iterate over
 * @param map_func The function that will be applied to each item
 * @return -1 on SUCCESS (yes I know).  When the function being applied
 * has a non-zero return Vec_map will return the index of the item that errored.
 */
int
T_Vec_map(T_Vec *v, void * map_arg, MapFunc map_func);

/**
 * This function uses a finder function to get an item that meets a criteria.
 * @param v The T_Vec to iterate over
 * @param find_func The function that will be used to find a result.
 * @return A pointer to the found item on success, NULL if no item was found.
 */
void *
T_Vec_find_ref(T_Vec *v, void *find_arg, FindFunc find_func);

/**
 * This function uses a finder function to get the index of an item that meets a criteria.
 * @param v The T_Vec to iterate over
 * @param find_func The function that will be used to find a result.
 * @return A ssize_t representing the index of the found item, -1 if no result was found.
 */
ssize_t
T_Vec_find_idx(T_Vec *v,  void *find_arg, FindFunc find_func);

/**
 * This function removes an item from the T_Vec by index
 * @param v the T_Vec to remove an item from
 * @param idx the index item to remove
 * @return Always returns EXIT_SUCCESS
 */
int
T_Vec_remove(T_Vec *v, size_t idx);

/**
 * This function locks the T_Vec mutex
 * @param v the T_Vec to lock
 * @return EXIT_SUCCESS or EXIT_FAILURE
 */
int
T_Vec_lock(T_Vec *v);

/**
 * This function unlocks the T_Vec mutex
 * @param v the T_Vec to unlock
 * @return EXIT_SUCCESS or EXIT_FAILURE
 */
int
T_Vec_unlock(T_Vec *v);

int
t_vec_enlarge(T_Vec *v);




#endif //VEC_T
